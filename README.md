Elegant Bedrooms, brings the ultimate luxury lifestyle experience courtesy of exceptional German and Italian design and craftsmanship. Our on-trend contemporary bedrooms and made-to-measure fitted and freestanding bedroom are designed bespoke to befit the most discerning tastes.

Address: Unit 1 Wylam Road, Mill Lane Trade Park, Liverpool L13 4BF, UK

Phone: +44 151 220 5550